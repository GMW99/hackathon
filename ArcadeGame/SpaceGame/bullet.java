import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class bullet here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class bullet extends Object
{
    /**
     * Act - do whatever the bullet wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    private int hp = 1;
    private int speed = 7;
    private int direction;
    private int damage = 15;
    public bullet(int dir){
        direction = dir;
    }
    public void act() 
    {
        setRotation(direction);
        moveAround();// Add your action code here.
        die();
    }    
    public void die(){
        Rock rock;
        rock = (Rock) getOneIntersectingObject(Rock.class);
        if (rock !=null)
        {
            rock.setHp(rock.getHp()-damage);
            this.setHp(this.getHp() -1);
            if (this.getHp() <= 0){
                getWorld().removeObject(this);
            }
        }
    }
    public void moveAround(){
        move(speed);
    }
}
