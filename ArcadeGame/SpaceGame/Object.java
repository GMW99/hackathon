import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Object here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public abstract class Object extends Actor
{
    private int speed;
    private int hp;
      
    public abstract void moveAround();
    public abstract void die();
    
    public void setSpeed(int spd){
        speed = spd;
    }   
    
    public int getSpeed(){
        return speed;
    }
    
        public void setHp(int health){
        hp = health;
    }   
    
    public int getHp(){
        return hp;
    }
}
