import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Rocket here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Rocket extends Object
{
    /**
     * Act - do whatever the Rocket wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    private int shotTimer = 0;
    private int hp = 5;
    private int speed = 4;
    public void act() 
    {
        moveAround();// Add your action code here.
        shoot();
    }    
    public void die(){}
    public void moveAround(){
        if (Greenfoot.isKeyDown("up")){
            move(speed);
        }
        if (Greenfoot.isKeyDown("down")){
            move(-speed);
        }
        if (Greenfoot.isKeyDown("left")){
            turn(5);
        }
        if (Greenfoot.isKeyDown("right")){
            turn(-5);
        }
    }
    public void shoot(){
        if (shotTimer > 0) {
            shotTimer = shotTimer - 1;
        }
        else if (Greenfoot.isKeyDown("space")) {
            getWorld().addObject(new bullet(getRotation()), getX(), getY());
            shotTimer = 10; // delay next shot
        }
        if (Greenfoot.isKeyDown("up")){
            World world;
            world = getWorld();
            world.addObject(new bullet(getRotation()), getX(), getY());
        }
    }
}
