import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Rock here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Rock extends Object
{
    /**
     * Act - do whatever the Rock wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public int hp = 2;
    private int speed = 1;
    private Rocket rocket = new Rocket();
    public void act() 
    {
        moveAround();// Add your action code here.
        
    }    
    public void die(){}
    public void setHp(int health){
        hp = health;
        if (hp <= 0){
            getWorld().removeObject(this);
        }
    }
    public void moveAround(){
        move (speed);
        // So if it is not there it stops
        if (getWorld().getObjects(Rocket.class).isEmpty()){
            return;
        }
        Actor rocket = (Actor)getWorld().getObjects(Rocket.class).get(0); 
        turnTowards(rocket.getX(),rocket.getY());
        
    }
}
